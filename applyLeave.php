<?php 
include_once('inc/header.php'); 

   $msg = '';
   $error = '';
   if (isset($_POST['apply_leave'])) {
     
      $valid = TRUE;

      if (empty($_POST['leave_type'])) {
        $error = "has-error";
        $valid = FALSE;
      }
      else{
        $leave_type = $_POST['leave_type'];
      }

      if (empty($_POST['from_date'])) {
        $error = "has-error";
        $valid = FALSE;
      }
      else{
        $from_date = $_POST['from_date'];
      }

      //to  date
      if (empty($_POST['to_date'])) {
        $error = "has-error";
        $valid = FALSE;
      }
      else{
        $to_date = $_POST['to_date'];
      }   

      //leave reason   
      if (empty($_POST['leave_reason'])) {
        $error = "has-error";
        $valid = FALSE;
      }
      else{
        $leave_reason = $_POST['leave_reason'];
      }   

      if ($valid) {
         $user_id = Session::get('id');
         $sql = "INSERT INTO apply_leave (user_id,leave_type,from_date,to_date,leave_reason) VALUES (?,?,?,?,?)";
         $data = array($user_id,$leave_type,$from_date,$to_date,$leave_reason);
         $status = $dbh->insertRow($sql,$data);
         if($status){
            $msg = "<div class='alert alert-success'>
            <strong>Applied for Leave but Pending!</strong>
         </div>";
         }     
      }      
   }
 ?>
    <div id="page-inner">
      <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">
                    Apply For Leave <small>Go for Masti!.</small>
                </h1>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <form class="form-horizontal" action="" method="post">

                          <div class="form-group <?php echo $error; ?>">
                            <label for="" class="col-sm-2 control-label">Leave Type</label>
                            <div class="col-sm-10">

                              <select class="form-control" name="leave_type">
                                <option value="">Select Leave Type</option>
                            <?php 

                              //vacation types retrieve
                              $sql =  "SELECT * FROM leave_types";
                              $results = $dbh->getRows($sql);

                              foreach ($results as $key => $value) {
                                ?>
                                    <option value="<?php echo $value['leave_id'] ?>"><?php echo $value['leave_type'] ?></option>
                                <?php
                              }

                             ?>
                              </select>
                            </div>
                          </div>

                          <div class="form-group <?php echo $error; ?>">
                            <label for="date" class="col-sm-2 control-label">From Date</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="from_date" id="datepicker">
                            </div>
                          </div>

                          <div class="form-group <?php echo $error; ?>">
                            <label for="toDate" class="col-sm-2 control-label">To Date</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="to_date" id="toDate">
                            </div>
                          </div>

                          <div class="form-group <?php echo $error; ?>">
                            <label for="comment" class="col-sm-2 control-label">Leave Reason</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" name="leave_reason" rows="3"></textarea>
                            </div>
                          </div>                          

                          <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                              <input type="submit" class="btn btn-default" name="apply_leave" value="Apply">
                            </div>
                          </div>
                        </form>                    
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <?php echo $msg; ?>
          </div>
        </div>
  </div>
     <!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>