<?php include_once('inc/header.php'); ?>
<div id="page-inner">
   <div class="row">
      <div class="col-md-12">
         <h1 class="page-header">
            Update Profile Info <small>Go for Masti!.</small>
         </h1>
      </div>
   </div>
   <!-- /. ROW  -->
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-body">

      <?php 
        $msg = '';
        $error = '';    

        if(isset($_POST['update_profile'])){

            $valid = TRUE;
            //full name
           if (empty($_POST['e_fname'])) {
             $error = "has-error";
             $valid = FALSE;
           }
           else{
            $e_fname = $_POST['e_fname'];
           }      

           //       
           if (empty($_POST['e_id'])) {
             $error = "has-error";
             $valid = FALSE;
           }
           else{
            $e_id = $_POST['e_id'];
           } 
           if (empty($_POST['e_email'])) {
             $error = "has-error";
             $valid = FALSE;
           }
           else{
            $e_mail = $_POST['e_email'];
           } 
           $user_id = Session::get('id');
           if ($valid) {
             $sql = "UPDATE employee SET e_fname = ?,e_id = ?,e_email = ? WHERE id= ?";
             $data = array($e_fname,$e_id,$e_mail,$user_id);
             $status = $dbh->updateRow($sql,$data);
               if($status == 1){
                  $msg = "<div class='alert alert-success'>
                  <strong>Updated!</strong>
               </div>";
               } 
               else{
                  $msg = "<div class='alert alert-warning'>
                  <strong>No update!</strong>
               </div>";
               }              
           }

        }    

      ?>

      <?php 
          //collection old values of profile
          $u_id = Session::get('id');
          //echo $u_id;
          $sql = "SELECT * FROM employee WHERE id= ?";
          $data = array($u_id);
          $result = $dbh->getRow($sql,$data);
          //var_dump($result);
          $old_fname = $result['e_fname'];
          $old_eid = $result['e_id'];
          $old_email = $result['e_email'];
      ?>

       <form action="" method="post">

          <div class="form-group">
             <label for="employee">Empployee Name</label>
             <input type="text" class="form-control" name="e_fname" value="<?php echo $old_fname ?>">
          </div>

          <div class="form-group">
             <label for="emp_id">Empployee ID</label>
             <input type="text" class="form-control" name="e_id" value="<?php echo $old_eid ?>">
          </div>

          <div class="form-group">
             <label for="email">Email</label>
             <input type="email" class="form-control" name="e_email" value="<?php echo $old_email ?>" >
          </div>


          <input type="submit" class="btn btn-default" name="update_profile" value="Update">
       </form>



            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-lg-6">
            <?php  echo $msg; ?>
      </div>
   </div>   
</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>