-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2018 at 05:23 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr_leave`
--

-- --------------------------------------------------------

--
-- Table structure for table `apply_leave`
--

DROP TABLE IF EXISTS `apply_leave`;
CREATE TABLE IF NOT EXISTS `apply_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `leave_type` varchar(30) NOT NULL,
  `from_date` varchar(30) NOT NULL,
  `to_date` varchar(30) NOT NULL,
  `leave_reason` text NOT NULL,
  `leave_status` enum('yes','no','deny') NOT NULL DEFAULT 'no',
  `status_seen` int(11) NOT NULL DEFAULT '0',
  `hr_comment` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apply_leave`
--

INSERT INTO `apply_leave` (`id`, `user_id`, `leave_type`, `from_date`, `to_date`, `leave_reason`, `leave_status`, `status_seen`, `hr_comment`) VALUES
(13, 12, '18', '12/13/2017', '12/20/2017', 'i need vacation moron hr', 'no', 1, NULL),
(14, 14, '17', '12/14/2017', '12/31/2017', 'moron hr', 'no', 0, NULL),
(15, 12, '18', '01/16/2018', '01/12/2018', 'jjjjjjjjjjj', 'no', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `e_fname` varchar(30) DEFAULT NULL,
  `e_id` varchar(50) DEFAULT NULL,
  `e_email` varchar(30) NOT NULL,
  `e_pass` varchar(60) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `e_fname`, `e_id`, `e_email`, `e_pass`, `role`) VALUES
(14, 'Empoyee3', 'Ash1311027m', 'employee34@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1),
(7, 'Subbir Hosain', 'Ash1311027m', 'subbir.ict@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 0),
(13, 'Subbir Hosain', 'ASh1311027m', 'employee2@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1),
(12, 'Subbir Hosain', 'Ash1311027m', 'employee@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1);

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

DROP TABLE IF EXISTS `leave_types`;
CREATE TABLE IF NOT EXISTS `leave_types` (
  `leave_id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_type` varchar(30) NOT NULL,
  PRIMARY KEY (`leave_id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`leave_id`, `leave_type`) VALUES
(20, 'Sabbatical leave'),
(19, 'Sick Leave'),
(18, 'Study Leave'),
(17, 'Special Leave'),
(21, 'Maternity Leave'),
(22, 'Vacation'),
(23, 'Fever');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
