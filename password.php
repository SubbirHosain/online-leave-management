<?php include_once('inc/header.php'); ?>
<div id="page-inner">
   <div class="row">
      <div class="col-md-12">
         <h1 class="page-header">
            Password Change <small>Go for Masti!.</small>
         </h1>
      </div>
   </div>
   <!-- /. ROW  -->
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-body">
  
    <?php 
         $msg = '';
         $error = '';
         //$wrong_oldpass = '';
         $pmatch='';
        if (isset($_POST['change_pass'])) {

             $valid = TRUE;

             //OLD PASSWORD CHECKING MECHANISM
             $user_id = Session::get('id');

             if (empty($_POST['oe_pass'])) {
               $error = "has-error";
               $valid = FALSE;
             }
             else{
              $oe_pass = md5($_POST['oe_pass']);
              
              $sql =  "SELECT * FROM employee WHERE id = ?";
              $data = array($user_id);
              $result = $dbh->getRow($sql,$data);
              $old_pass = $result['e_pass'];
              if ($oe_pass!=$old_pass) {
                $valid = FALSE;
                $wrong_oldpass=" old Password didn't match!";
              }               
             }    

             //new password
             if (empty($_POST['ne_pass'])) {
               $error = "has-error";
               $valid = FALSE;
             }
             else{
              $ne_pass = $_POST['ne_pass'];
             } 

            //confirm password
             if (empty($_POST['cone_pass'])) {
               $error = "has-error";    
               $valid = FALSE;           
             }
             else{
              $cone_pass = $_POST['cone_pass'];

             }
            if ($_POST['ne_pass']!=$_POST['cone_pass']) {
              $pmatch = "password didn't match!";
              $valid = FALSE;
            }
            else{
              $final_pass = md5($_POST['cone_pass']);
            }
            
             
            if ($valid) {
              $sql = "UPDATE `employee` SET e_pass = ? WHERE id = ?";
              $data = array($final_pass,$user_id);
              $status = $dbh->updateRow($sql,$data);
               if($status==1){
                  $msg = "<div class='alert alert-success'>
                  <strong>Updated!</strong>
               </div>";
               } 
               else{
                  $msg = "<div class='alert alert-warning'>
                  <strong>No update!</strong>
               </div>";
               } 
            }

        }
     ?>

               <form action="" method="post">

                  <div class="form-group">
                     <label for="opass">Old Password</label>
                     <input type="password" class="form-control" name="oe_pass" placeholder="old password">
                     <p class="help-block"><?php echo $a = (isset($wrong_oldpass)) ? $wrong_oldpass : '' ; ?></p>
                  </div>

                  <div class="form-group">
                     <label for="new_pass">New Password</label>
                     <input type="password" class="form-control" name="ne_pass" id="new_pass" placeholder="Password">
                  </div>
                  <div class="form-group">
                     <label for="con_pass">Confirm Password</label>
                     <input type="password" class="form-control" name="cone_pass" id="con_pass" placeholder="Confirm Password">
                     <p class="help-block"><?php echo $pmatch; ?></p>
                  </div>
                  <input type="submit" class="btn btn-default" name="change_pass" value="Save">
               </form>


            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-6">
            <?php  echo $msg; ?>
      </div>
   </div>
</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>