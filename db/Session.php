<?php
class Session
{
	public static function init(){
		session_start();
	}
	public static function set($key, $value)
	{
		$_SESSION[$key] = $value;
	}

	public static function get($key)
	{
		if (isset($_SESSION[$key])) {
			return $_SESSION[$key];
		}
		else
		{
			return false;
		}
	}

	//checking if loggged in header
	public static function checkSession(){
		self::init();
		if (self::get("login") == false) {
			self::destroy();
			header("location:login.php");
		}
	}


	//if already loged in redirect at login page
	public static function checkLogin(){
		self::init();
		if (self::get("login") == true) {
			header("location:index.php");
		}
	}
	public static function destroy(){
		session_destroy();
		header("location:login.php");
	}
}
?>