<?php 
ob_start();
include('db/Session.php');
Session::checkSession();
include('db/database.php');
$dbh = new Database(); 
 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LICT Project Online HR Leave Management</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- jQeury Defatult Styles -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Lict Project</a>
            </div>
            <!-- notification -->
            <ul class="nav navbar-top-links navbar-right">
<?php 
    $user_id = Session::get('id');
    $yes = 'yes';
    $seenId = 0;
    $sql_num =  "SELECT al.id,al.from_date,al.to_date,al.leave_reason,al.leave_status,al.status_seen,al.hr_comment,emp.e_fname,emp.e_id,ltyp.leave_type
              FROM apply_leave al 
                    INNER JOIN employee emp ON al.user_id = emp.id 
                    INNER JOIN leave_types ltyp ON al.leave_type = ltyp.leave_id
            WHERE al.user_id = ? AND (al.leave_status = ? AND al.status_seen = ?)"; 

     $data2 = array($user_id,$yes,$seenId);
     $num = $dbh->rowCounts($sql_num,$data2);
    
     //formaking notification hidden
    $seen_id = 1;
?>            
                <li class="dropdown">
                    <a  href="seeNotification.php?seen=<?php echo $seen_id ?>" aria-expanded="false">
                        <i class="fa fa-bell fa-fw"></i> 
                        <span  style="background-color:#5bc0de;border-color: #46b8da;color: #fff;">
                            <?php 
                                if ($num!=0) {
                                    echo $num;
                                }
                            ?>
                            
                        </span>
                    </a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
<?php 
$user_id = Session::get('id');
$sql =  "SELECT * FROM employee WHERE id = ?";
$data = array($user_id);
$result = $dbh->getRow($sql,$data);
?>  

                        <li><a href="userProfile.php"><i class="fa fa-user fa-fw"></i> <?php echo $result['e_fname']; ?></a>
                        </li>
                        <li class="divider"></li>

                        <?php
                            if (isset($_GET['action']) && $_GET['action'] == "logout" ) {
                                Session::destroy();
                            }
                         ?>
                                                 
                        <li><a href="?action=logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="deleteRequest.php"><i class="fa fa-fw fa-file"></i> Delete Request</a>
                    </li>
                    <li>
                        <a href="updateRequest.php"><i class="fa fa-fw fa-file"></i> Update Request</a>
                    </li>                    
                    <li>
                        <a href="leaveStatus.php"><i class="fa fa-suitcase"></i> Leave Status</a>
                    </li>
                    <li>
                        <a href="applyLeave.php"><i class="fa fa-suitcase"></i> Apply Leave</a>
                    </li>
                    <li>
                        <a href="takenLeaves.php"><i class="fa fa-suitcase"></i>Taken Leave List</a>
                    </li>
                    <li>
                        <a href="userProfile.php"><i class="fa fa-suitcase"></i> Update Profile</a>
                    </li>
                    <li>
                        <a href="password.php"><i class="fa fa-suitcase"></i> Change Password</a>
                    </li>                    
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">