<?php include_once('inc/header.php'); ?>
<div id="page-inner">
<div class="row">
   <div class="col-md-12">
      <h1 class="page-header">
         Apply For Leave <small>Go for Masti!.</small>
      </h1>
   </div>
</div>
<!-- /. ROW  -->
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
   <div class="panel-body">
      <table class="table table-bordered">
      <thead>
         <tr>
            <th>Full Name</th>
            <th>Username</th>
            <th>Type</th>
            <th>Duration</th>
            <th>Details</th>
            <th>Actions</th>
         </tr>
      </thead>
      <tbody>
  <?php 
     //include('../db/database.php');
     //$dbh = new Database(); 

    $user_id = Session::get('id');
    $no = 'no';  
     $sql =  "SELECT al.id,al.from_date,al.to_date,al.leave_reason,al.leave_status,al.hr_comment,emp.e_fname,emp.e_id,ltyp.leave_type
              FROM apply_leave al 
                    INNER JOIN employee emp ON al.user_id = emp.id 
                    INNER JOIN leave_types ltyp ON al.leave_type = ltyp.leave_id
            WHERE al.user_id = ? AND leave_status = ?"; 

     $data = array($user_id,$no);
     $results = $dbh->getRows($sql,$data);
     foreach ($results as $key => $value) {?>     
         <tr>
            <td>
               <?php echo $value['e_fname']; ?>
            </td>
            <td><?php echo $value['e_id'] ?></td>
            <td><?php echo $value['leave_type'] ?></td>
            <td>
               <?php echo $value['from_date'] ?> <br>
               to <br>
               <?php echo $value['from_date'] ?>
            </td>
            <td>
              <?php echo $value['leave_reason']; ?>
            </td>
            <td>
               <a href="editLeave.php?id=<?php echo $value['id'] ?>" class="btn btn-warning">Edit</a>
            </td>
         </tr>
      <?php }   
  ?>         
      </tbody>
   </div>
</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>