<?php 
include_once('inc/header.php'); 

   $msg = '';
   $error = '';
   if(isset($_POST['add_leave'])){

      //taking input from form fields
      $sick_type = $_POST['sick_type'];
      $id = $_GET['id'];
      $valid = TRUE;

      if (empty($sick_type)) {
         $error = "has-error";
         $valid = FALSE;
      }

      if ($valid) {
         
         $sql = "UPDATE `leave_types` SET leave_type = ? WHERE leave_id = ?";
         $data = array($sick_type,$id);
         $status = $dbh->updateRow($sql,$data);
         if($status==1){
            $msg = "<div class='alert alert-success'>
            <strong>Updated!</strong>
         </div>";
         } 
         else{
            $msg = "<div class='alert alert-warning'>
            <strong>No update!</strong>
         </div>";
         }    
      }
   }

 ?>
<div id="page-inner">
   <div class="row">
      <div class="col-md-12">
         <h1 class="page-header">
            Add Leave Types <small>Manage Leave Types!</small>
         </h1>
      </div>
   </div>
   <!-- /. ROW  -->
   <div class="row">
      <div class="col-lg-6">
         <?php 
            //collecting old values
            if(isset($_GET['id'])){

               $id = $_GET['id'];
               $sql =  "SELECT * FROM leave_types WHERE leave_id = ?";
               $data = array($id);
               $result = $dbh->getRow($sql,$data);
               
               $oid = $result['leave_id'];
               $old_leave_type = $result['leave_type'];
            }
          ?>
         <form class="" action="" method="post">
            <div class="input-group <?php echo $error; ?>">
               <input type="text" class="form-control" name="sick_type" value="<?php echo $retVal = (isset($old_leave_type)) ? $old_leave_type : '' ; ?>"">
               <span class="input-group-btn">
               <input type="hidden" name="id" value="<?php echo $retVal = (isset($oid)) ? $oid : '' ; ?>">
               <input type="submit" class="btn btn-default" name="add_leave" value="Update Leave Type!">
               </span>
            </div>
            <!-- /input-group -->
         </form>
      </div>
      <!-- /.col-lg-6 -->
   </div>
   <div class="row">
      <div class="col-lg-6">
            <?php  echo $msg; ?>
      </div>
   </div>

</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>