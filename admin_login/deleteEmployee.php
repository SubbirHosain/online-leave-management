<?php 
ob_start();
include_once('inc/header.php'); 

if (isset($_GET['id'])) {
   
   $delId = $_GET['id'];

   $sql = "DELETE FROM employee WHERE id=?";
   $data = array($delId);
   $dbh->deleteRow($sql,$data);
   header("location:employeeList.php?msg=Deleted");


}
else{
    header("location:index.php");
}
?>