<?php 
include_once('inc/header.php');

if (isset($_GET['id'])) {
   
   $updateId = $_GET['id'];
   $yes = 'yes';
   $sql = "UPDATE `apply_leave` SET leave_status = ? WHERE id = ?";
   $data = array($yes,$updateId);
   $status = $dbh->updateRow($sql,$data);
   
   if ($status) {
       header("location:rejected.php?msg=Approved");
   }

}
else{
    header("location:index.php");
}
?>