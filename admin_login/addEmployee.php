<?php include_once('inc/header.php'); ?>
<?php 

   //include('../db/database.php');
   //$dbh = new Database();

   $msg = '';
   $error = '';
   $pmatch = '';
   $email_exists = '';

   if (isset($_POST['add'])) {

     $valid = TRUE;

/*     if (empty($_POST['fname'])) {
       $error = "has-error";
       $valid = FALSE;
     }
     else{
      $e_fname = $_POST['fname'];
     }

     if (empty($_POST['e_id'])) {
       $error = "has-error";
       $valid = FALSE;
     }
     else{
      $e_id = $_POST['e_id'];
     } */

     if (empty($_POST['e_email'])) {
       $error = "has-error";
       $valid = FALSE;
     }
     else{
      $e_email = $_POST['e_email'];
      $sql = "SELECT * FROM employee WHERE e_email=?";
      $data = array($e_email);
      $num = $dbh->rowCounts($sql,$data);
      if ($num>0) {
        $email_exists = 'This email already exists';
        $valid = FALSE;
      }
     }     

     if (empty($_POST['e_pass'])) {
       $error = "has-error";
       $valid = FALSE;
     }
     else{
      $e_pass= $_POST['e_pass'];
     }  

     if (empty($_POST['e_cpass'])) {
       $error = "has-error";
       $valid = FALSE;
     }
     else{
      $e_cpass= $_POST['e_cpass'];
     }

      if ($_POST['e_pass']!=$_POST['e_cpass']) {
         $error = "has-error";
         $valid = FALSE;  
         $pmatch="Password didn't match!";     
      }   
      else{
        $final_pass = md5($_POST['e_cpass']);
      }  


      $role= $_POST['role'];

      if ($valid) {
         
         $sql = "INSERT INTO employee (e_email,e_pass,role) VALUES (?,?,?)";
         $data = array($e_email,$final_pass,$role);
         $status = $dbh->insertRow($sql,$data);
         if($status){
            $msg = "<div class='alert alert-success'>
            <strong>Employee added!</strong>
         </div>";
         }     
      } 

   }

 ?>
<div id="page-inner">
   <div class="row">
      <div class="col-md-12">
         <h1 class="page-header">
            Add Employee <small>Go for Masti!.</small>
         </h1>
      </div>
   </div>
   <!-- /. ROW  -->
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-body">

               <form action="" method="post">

<!--                   <div class="form-group <?php  $error; ?>">
                     <label for="employee">Empployee Name</label>
                     <input type="text" class="form-control" name="fname" id="employee" placeholder="Full Name">
                  </div> -->

<!--                   <div class="form-group <?php  $error; ?>">
                     <label for="emp_id">Empployee ID</label>
                     <input type="text" class="form-control" id="emp_id" name="e_id" placeholder="id">
                  </div> -->

                  <div class="form-group <?php echo $error; ?>">
                     <label for="email">Email</label>
                     <input type="email" class="form-control" name="e_email" id="email" placeholder="Email">
                     <p class="help-block"><?php echo $email_exists; ?></p>
                  </div>

                  <div class="form-group <?php echo $error; ?>">
                     <label for="pass">Password</label>
                     <input type="password" class="form-control" name="e_pass" id="pass" placeholder="Password">
                  </div>

                  <div class="form-group <?php echo $error; ?>">
                     <label for="c_pass">Confirm Password</label>
                     <input type="password" class="form-control" name="e_cpass" id="c_pass" placeholder="Confirm Password">
                     <p class="help-block"><?php echo $pmatch; ?></p>
                  </div>
                  <div class="form-group <?php echo $error; ?>"">
                     <label>Select Role</label>
                     <select class="form-control" name="role">
                        <option value="">Select</option>
                        <option value="0">Hr</option>
                        <option value="1">Employee</option>
                     </select>
                  </div>
                  <input type="submit" class="btn btn-default" name="add" value="Add!">

               </form>
            </div>
         </div>
      </div>
   </div>
  <div class="row">
    <div class="col-lg-6">
      <?php echo $msg; ?>
    </div>
  </div>   
</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>