<?php 
include_once('inc/header.php');

   $msg = '';
   $error = '';
   if (isset($_POST['add_leave'])) {

      //taking input from form fields
      $sick_type = $_POST['sick_type'];
      $valid = TRUE;

      if (empty($sick_type)) {
         $error = "has-error";
         $valid = FALSE;
      }

      if ($valid) {
         
         $sql = "INSERT INTO leave_types (leave_type) VALUES (?)";
         $data = array($sick_type);
         $status = $dbh->insertRow($sql,$data);
         if($status){
            $msg = "<div class='alert alert-success'>
            <strong>Added!</strong>
         </div>";
         }     
      }
   }

 ?>
<div id="page-inner">
   <div class="row">
      <div class="col-md-12">
         <h1 class="page-header">
            Add Leave Types <small>Manage Leave Types!</small>
         </h1>
      </div>
   </div>
   <!-- /. ROW  -->
   <div class="row">
      <div class="col-lg-6">
         <form class="" action="" method="post">
            <div class="input-group <?php echo $error; ?>">
               <input type="text" class="form-control" name="sick_type" placeholder="Fever">
               <span class="input-group-btn">
               <input type="submit" class="btn btn-default" name="add_leave" value="Add New Leave Type!">
               </span>
            </div>
            <!-- /input-group -->
         </form>
      </div>
      <!-- /.col-lg-6 -->
   </div>
   <div class="row">
      <div class="col-lg-6">
            <?php  echo $msg; ?>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-6">
         <div class="panel panel-default">
            <div class="panel-body">
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th>Leave Type</th>
                        <th>Actions</th>
                     </tr>
                  </thead>
                  <tbody>

                     <?php 
                        $sql =  "SELECT * FROM leave_types";
                        $results = $dbh->getRows($sql);
                        foreach ($results as $key => $value) {
                           ?>
                           <tr>
                              <td><?php echo $value['leave_type']; ?></td>
                              <td>
                                 <a href="editLeaveTypes.php?id=<?php echo $value['leave_id'] ?>" class="btn btn-default">Edit</a>
                                 <a href="delLeaveTypes.php?id=<?php echo $value['leave_id'] ?>" class="btn btn-default">Delete</a>
                              </td>
                           </tr>
                           <?php
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>