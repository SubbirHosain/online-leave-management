<?php 
include_once('inc/header.php');

if (isset($_GET['id'])) {
   
   $updateId = $_GET['id'];
   $no = 'deny';
   $sql = "UPDATE `apply_leave` SET leave_status = ? WHERE id = ?";
   $data = array($no,$updateId);
   $status = $dbh->updateRow($sql,$data);
   
   if ($status) {
       header("location:approved.php?msg=Denied");
   }

}
else{
    header("location:index.php");
}
?>