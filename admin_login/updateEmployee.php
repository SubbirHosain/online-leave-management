<?php include_once('inc/header.php'); ?>
            <div id="page-inner">
      <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Update Employee Info <small>Go for Masti!.</small>
                        </h1>
                    </div>
                </div>
                 <!-- /. ROW  -->
            <div class="row">
                        <div class="col-lg-6">
                                    <div class="panel panel-default">
                                                <div class="panel-body">
                                                <form action="" method="post">
                                                  <div class="form-group">
                                                    <label for="employee">Empployee Name</label>
                                                    <input type="text" class="form-control" id="employee" value="Old value">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="emp_id">Empployee ID</label>
                                                    <input type="text" class="form-control" id="emp_id" >
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="email" class="form-control" id="email" >
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="pass">Password</label>
                                                    <input type="password" class="form-control" id="pass" >
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="c_pass">Confirm Password</label>
                                                    <input type="password" class="form-control" id="c_pass">
                                                  </div>
                                                  <button type="submit" class="btn btn-default">Submit</button>
                                                </form>
                                    </div>
                                    </div>
                        </div>
            </div>
  </div>
             <!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>