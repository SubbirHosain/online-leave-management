<?php
ob_start();
include('../db/Session.php');
Session::checkSession();
include('../db/database.php');
$dbh = new Database();
 ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>LICT Project Online HR Leave Management ICE NSTU</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Lict Project</a>
            </div>
            <!-- notification -->
            <ul class="nav navbar-top-links navbar-right">
<?php 
    $no = 'no';
    $sql =  "SELECT * FROM apply_leave WHERE leave_status = ?";
    $data = array($no);
    $num = $dbh->rowCounts($sql,$data);
?>

                <li>
                    <a href="requestLists.php" aria-expanded="false">
                        <i class="fa fa-bell fa-fw"></i> 
                        <span  style="background-color:#5bc0de;border-color: #46b8da;color: #fff;">
                        
                        <?php 

                            if ($num!=0) {
                                echo $num;
                            }
                        ?>
                            
                        </span>
                    </a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">

<?php 
    $user_id = Session::get('id');
    $sql =  "SELECT * FROM employee WHERE id = ?";
    $data = array($user_id);
    $result = $dbh->getRow($sql,$data);
?>                    
                        <li><a href="userProfile.php"><i class="fa fa-user fa-fw"></i> <?php echo $result['e_fname']; ?></a>
                        </li>
                        <li class="divider"></li>
                        <?php
                            if (isset($_GET['action']) && $_GET['action'] == "logout" ) {
                                Session::destroy();
                            }
                         ?>                        
                       <li><a href="?action=logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a class="active-menu" href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a href="leaveTypes.php"><i class="fa fa-fw fa-file"></i> Add Leave Type</a>
                    </li>
                    <li>
                        <a href="addEmployee.php"><i class="fa fa-suitcase"></i> Add New Employee</a>
                    </li>
                    <li>
                        <a href="employeeList.php"><i class="fa fa-suitcase"></i> Employee List</a>
                    </li>  
                    <li>
                        <a href="password.php"><i class="fa fa-suitcase"></i> Password Change</a>
                    </li>
                    <li>
                        <a href="requestLists.php"><i class="fa fa-suitcase"></i> Request Lists</a>
                    </li>
                    <li>
                        <a href="approved.php"><i class="fa fa-suitcase"></i> Approved </a>
                    </li>
                    <li>
                        <a href="rejected.php"><i class="fa fa-suitcase"></i> Rejected</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">