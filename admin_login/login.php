<?php 
    ob_start();
   include '../db/Session.php'; 
   Session::checkLogin();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>LICT Project Online HR Leave Management</title>
      <!-- Bootstrap Styles-->
      <link href="assets/css/bootstrap.css" rel="stylesheet" />
      <!-- FontAwesome Styles-->
      <link href="assets/css/font-awesome.css" rel="stylesheet" />
      <!-- Custom Styles-->
      <link href="assets/css/custom-styles.css" rel="stylesheet" />
      <!-- Google Fonts-->
      <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
   </head>
   <body>
      <div id="wrapper">
         <div id="page-wrapper">
            <div id="page-inner">
               <div class="row">
                  <div class="col-md-4 col-md-offset-3">
                     <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                           <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">

<?php 
include('../db/database.php');
$dbh = new Database();

      $msg = '';
      $error = '';

      if (isset($_POST['user_login'])) {

         $valid = TRUE;

         if (empty($_POST['e_email'])) {
           $error = "has-error";
           $valid = FALSE;
         }
         else{
           $e_email = $_POST['e_email'];
         }    

         if (empty($_POST['e_pass'])) {
           $error = "has-error";
           $valid = FALSE;
         }
         else{
           $e_pass = md5($_POST['e_pass']);
         }  

         if ($valid) {
            $sql = "SELECT * FROM employee WHERE e_email = ? AND e_pass= ?";
            $data = array($e_email,$e_pass);

            $result = $dbh->getRow($sql,$data);
            $role = $result['role'];
            $num = $dbh->rowCounts($sql,$data);

            if($num>0 AND $role== '0'){
               Session::set("login", true);
               Session::set("id", $result['id']);
               Session::set("e_fname", $result['e_fname']);
               Session::set("e_id", $result['e_id']);
               Session::set("e_email", $result['e_email']);
               Session::set("userRole", $result['role']);
               if (!empty($_POST['remember_me'])) {
                 setcookie ("member_login",$_POST["e_email"],time()+ (10 * 365 * 24 * 60 * 60));
                 setcookie ("member_password",$_POST["e_pass"],time()+ (10 * 365 * 24 * 60 * 60));
               }
               else{
                  if(isset($_COOKIE["member_login"])){
                     setcookie ("member_login","");
                  }
                  if(isset($_COOKIE["member_password"])) {
                     setcookie ("member_password","");
                  }                  
               }
               //redirect the user
               header("location:index.php");
            }
            else{
               $msg = "<div class='alert alert-danger'>
                        <strong>Either email or password wrong</strong>
                     </div>";
            }
            

         }

      }

   ?>


      <form role="form" method="post">
         <fieldset>
            <div class="form-group">
               <input class="form-control" placeholder="E-mail" name="e_email" type="email" value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["member_login"]; } ?>">
            </div>
            <div class="form-group">
               <input class="form-control" placeholder="Password" name="e_pass" type="password" value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>">
            </div>
            <div class="checkbox">
               <label>
               <input  type="checkbox" name="remember_me" <?php if(isset($_COOKIE["member_login"])) { ?> checked <?php } ?> >Remember Me
               </label>
            </div>
            <!-- Change this to a button or input when using this as a form -->
            <input type="submit" class="btn btn-lg btn-success btn-block"  name ="user_login" value="Login">


         </fieldset>
      </form>


                        </div>
                     </div>
                  </div>
               </div>

      <div class="row">
         <div class="col-lg-6">
            <?php echo $msg ?>
         </div>
      </div>
            </div>
            <!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>