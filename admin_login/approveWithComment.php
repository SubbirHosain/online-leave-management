<?php 
ob_start();
include_once('inc/header.php'); ?>
<?php 
  
  if (isset($_POST['approve_leave'])) {
    
    $hr_comment = $_POST['hr_comment'];
    $updateId = $_GET['id'];
    $yes = 'yes';
   $sql = "UPDATE `apply_leave` SET leave_status = ?,hr_comment = ? WHERE id = ?";
   $data = array($yes,$hr_comment,$updateId);
   $status = $dbh->updateRow($sql,$data);
   
   if ($status) {
       header("location:requestLists.php?msg=Approved");
    }  
  }

?>
  <div id="page-inner">
      <div class="row">
          <div class="col-md-12">
              <h1 class="page-header">
                Approve with Comment <small>Go for Masti!.</small>
              </h1>
          </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-body">

  <?php 
      if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $sql = "SELECT * FROM apply_leave WHERE id=?";
        $data = array($id);
        $result = $dbh->getRow($sql,$data);

        $old_leaveType_id = $result['leave_type'];
        $old_fromDate = $result['from_date'];
        $old_toDate = $result['to_date'];
        $old_leaveReason = $result['leave_reason'];
      }
  ?>
    <form class="form-horizontal" action="" method="post">

      <div class="form-group">
        <label for="" class="col-sm-2 control-label">Leave Type</label>
        <div class="col-sm-10">

          <select class="form-control" name="leave_type" disabled="disabled">
            <option value="">Select Leave Type</option>
        <?php 

          //vacation types retrieve
          $sql =  "SELECT * FROM leave_types";
          $results = $dbh->getRows($sql);

          foreach ($results as $key => $value) {
            ?>
              
                <option <?php if($value['leave_id']==$old_leaveType_id) { echo 'selected';}?> value="<?php echo $value['leave_id'] ?>"> <?php echo $value['leave_type'] ?></option>
            <?php
          }

         ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="date" class="col-sm-2 control-label">From Date</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" disabled value="<?php echo $old_fromDate;  ?>" name="from_date" id="datepicker">
        </div>
      </div>

      <div class="form-group">
        <label for="toDate" class="col-sm-2 control-label">To Date</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" disabled value="<?php echo $old_toDate;  ?>" name="to_date" id="toDate">
        </div>
      </div>

      <div class="form-group">
        <label for="comment" class="col-sm-2 control-label">Leave Reason</label>
        <div class="col-sm-10">
          <textarea class="form-control" disabled="disabled" name="leave_reason" rows="3"><?php echo $old_leaveReason; ?></textarea>
        </div>
      </div> 

      <div class="form-group">
        <label for="comment" class="col-sm-2 control-label">HR Comment</label>
        <div class="col-sm-10">
          <textarea class="form-control"  name="hr_comment" rows="3"></textarea>
        </div>
      </div>  

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <input type="submit" class="btn btn-success" name="approve_leave" value="Approve">
        </div>
      </div>
    </form>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>