<?php include_once('inc/header.php'); ?>
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Dashboard <small>Summary of Leave Management</small>
                        </h1>
                    </div>
                </div>
                <!-- /. ROW  -->
<?php 
    $yes = 'yes';
    $sql =  "SELECT * FROM apply_leave WHERE leave_status = ?";
    $data = array($yes);
    $approved_no = $dbh->rowCounts($sql,$data);
?> 
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="panel panel-primary text-center no-boder bg-color-green">
                            <div class="panel-body">
                                <i class="fa fa-bar-chart-o fa-5x"></i>
                                <h3><?php echo $approved_no; ?></h3>
                            </div>
                            <div class="panel-footer back-footer-green">
                                Approved Leaves
                            </div>
                        </div>
                    </div>

<?php 
    $deny = 'deny';
    $sql =  "SELECT * FROM apply_leave WHERE leave_status = ?";
    $data = array($deny);
    $rejected_no = $dbh->rowCounts($sql,$data);
?> 
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="panel panel-primary text-center no-boder bg-color-red">
                            <div class="panel-body">
                                <i class="fa fa fa-comments fa-5x"></i>
                                <h3><?php echo $rejected_no; ?> </h3>
                            </div>
                            <div class="panel-footer back-footer-red">
                                Rejected Leaves
                            </div>
                        </div>
                    </div>
<?php 
    $no = 'no';
    $sql =  "SELECT * FROM apply_leave WHERE leave_status = ?";
    $data = array($no);
    $deny_no = $dbh->rowCounts($sql,$data);
?>                    
                <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="panel panel-primary text-center no-boder bg-color-brown">
                            <div class="panel-body">
                                <i class="fa fa-users fa-5x"></i>
                                <h3><?php echo $deny_no; ?></h3>
                            </div>
                            <div class="panel-footer back-footer-brown">
                                Pending Reqeusts

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>