<?php include_once('inc/header.php'); ?>
<div id="page-inner">
<div class="row">
   <div class="col-md-12">
      <h1 class="page-header">
          Employee Lists <small>Go for Masti!.</small>
      </h1>
   </div>
</div>
<!-- /. ROW  -->
<div class="row">
<div class="col-lg-12">
<div class="panel panel-default">
   <div class="panel-body">
      <table class="table table-bordered">
      <thead>
         <tr>
            <th>Full Name</th>
            <th>Employee ID</th>
            <th>Email</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>

  <?php 
     //include('../db/database.php');
     //$dbh = new Database(); 
     $role = '1'; 
     $sql =  "SELECT * FROM employee WHERE role = ?"; 
     $data = array($role);
     $results = $dbh->getRows($sql,$data);
     //var_dump($results);
     foreach ($results as $key => $value) {?>
       <tr>
          <td>
            <?php echo $value['e_fname']; ?>
          </td>
          <td><?php echo $value['e_id'] ?></td>
          <td><?php echo $value['e_email'] ?></td>
          <td>
          <a href="editEmployee.php?id=<?php echo $value['id'] ?>"  class="btn btn-success">Edit</a>
          <a href="deleteEmployee.php?id=<?php echo $value['id'] ?>"  class="btn btn-danger">Delete</a>
          </td>
       </tr>          
      <?php }   
  ?>

      </tbody>
  </table>
   </div>
</div>
<!-- /. PAGE INNER  -->
<?php include_once('inc/footer.php'); ?>